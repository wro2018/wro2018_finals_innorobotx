WRO2018 ARC TETRIS Autonomous Robot Labview documentation

# Program structure

The RT main program is divised into several parallel while loops which are hidden into SubVIs. Every parallel running SubVI has a different task.

## SubVIs of RT main VI 

1. Camera
2. 3D Motion
3. Servo operator
4. Statemachine